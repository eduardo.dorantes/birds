import React from "react";
import { View, Text } from "react-native";
import { createAppContainer } from "react-navigation";
import AppNavigator from './navigation';
import AuthScreen from './screens/AuthScreen';
import firebase from 'react-native-firebase';

export default class EntryPoint extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			user:			null
		}
		this.unsubscriber = null;
	}
	
	componentDidMount(){
		this.unsubscriber = firebase.auth().onAuthStateChanged((user) => {
			this.setState({ user });
		});
	}
	
	componentWillUnmount(){
		if (this.unsubscriber) {
			this.unsubscriber();
		}
	}
	
	render(){
		if(!this.state.user){
			return <AuthScreen/>
		}
		else{
			return(
				<AppNavigator/>
			);
		}
	}
}