const functions = require("firebase-functions");
const admin		= require("firebase-admin");
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.inAppgetAllUsers = functions.https.onCall((data, context) => {
	return admin.auth().listUsers().then((userRecords) => {
		var usersData = [];
		userRecords.users.forEach((user) => {
			usersData.push({
				uid:			user.uid,
				email:			user.email,
				displayName:	user.displayName,
				emailVerified:	user.emailVerified,
				metadata:		user.metadata
			});
		});
		return usersData;
	})
	.catch((error) => {
		return error;
	});
});

exports.inAppActivateUser = functions.https.onCall((data, context) => {
	const uid = data.uid;
	return admin.auth().updateUser(uid, {
		emailVerified: true,
		//disabled: true
	})
	.then(function(userRecord) {
		return true;
	})
	.catch(function(error) {
		return false;
	});
});

exports.inAppDeactivateUser = functions.https.onCall((data, context) => {
	const uid = data.uid;
	return admin.auth().updateUser(uid, {
		emailVerified: false,
		//disabled: true
	})
	.then(function(userRecord) {
		return true;
	})
	.catch(function(error) {
		return false;
	});
});

exports.getAllUsers = functions.https.onRequest((req, res) => {
	admin.auth().listUsers().then((userRecords) => {
		var usersData = [];
		userRecords.users.forEach((user) => {
			usersData.push({
				uid:			user.uid,
				email:			user.email,
				displayName:	user.displayName,
				emailVerified:	user.emailVerified,
				metadata:		user.metadata
			});
		});
		return res.send(JSON.stringify(usersData));
	})
	.catch((error) => {
		res.send(JSON.stringify(error));
	});
});