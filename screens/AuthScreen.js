import React from 'react';
import { View, Text, Image, Button, Modal, Alert, StatusBar, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import Icon from '../components/Icon';
import Layout from '../constants/Layout';
import firebase from 'react-native-firebase';

export default class AuthScreen extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			modalVisible:		false,
			email:				'',
			password:			'',
			password_confirm:	'',
			name:				'',
			viewMode:			0
		}
		this.registerUser	= this.registerUser.bind(this);
		this.signInUser		= this.signInUser.bind(this);
	}
	
	componentDidMount(){
		
	}
	
	componentWillUnmount(){
		
	}
	
	registerUser = () => {
		const name		= this.state.name;
		const email		= this.state.email;
		const password	= this.state.password;
		const password_confirm	= this.state.password_confirm;
		if(password.trim() != password_confirm.trim()){
			Alert.alert("Tu contraseña y su verificación no coinciden.");
			return false;
		}
		else if(password.length < 8){
			Alert.alert("Tu contraseña debe ser mayor a 8 dígitos.");
			return false;
		}
		else{
			firebase.auth().createUserWithEmailAndPassword(email, password)
			.then((userCredential) => {
				firebase.auth().currentUser.updateProfile({displayName: name});
			})
			.catch((error) => {
				Alert.alert("Error al registrarte", error.message);
			})
		}
	}
	
	signInUser = () => {
		const email		= this.state.email;
		const password	= this.state.password;
		firebase.auth().signInWithEmailAndPassword(email, password)
		.then((userCredential) => {
			
		})
		.catch((error) => {
			Alert.alert("Error al acceder", error.message);
		});
	}
	
	render() {
		return (
			<View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
				<Modal
					animationType="slide"
					transparent={false}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						this.setState({modalVisible: false});
					}}
				>
					<StatusBar hidden={true}/>
					<ScrollView style={{width: '100%', height: '100%', backgroundColor: 'white'}}>
						<View style={{width: '100%', alignItems: 'center'}}>
							<Image source={require('../assets/pictures/bird.png')} style={{width: '50%', height: Layout.window.width/2, margin: 10}}/>
							<Text>Biomedical Issue Reporting Digital System</Text>
							<Text style={{fontWeight: 'bold'}}>B.I.R.D.S.</Text>
						</View>
						<View style={{width: '100%', height: 25, marginTop: 10, marginBottom: 10, flexDirection: 'row'}}>
							<TouchableOpacity onPress={() => { this.setState({viewMode: 0}) }} style={{width: '40%', borderBottomWidth: (this.state.viewMode == 0 ? 1 : 0), borderBottomColor: 'gray', marginLeft: '5%', marginRight: '5%'}}>
								<Text style={{textAlign: 'center'}}>Quiero registrarme</Text>
							</TouchableOpacity>
							<TouchableOpacity onPress={() => { this.setState({viewMode: 1}) }} style={{width: '40%', borderBottomWidth: (this.state.viewMode == 1 ? 1 : 0), borderBottomColor: 'gray', marginLeft: '5%', marginRight: '5%'}}>
								<Text style={{textAlign: 'center'}}>Ya soy usuario</Text>
							</TouchableOpacity>
						</View>
						{this.state.viewMode == 0 &&
							<View style={{padding: 10, width: '100%'}}>
								<View style={{margin: 5}}>
									<TextInput
										placeholder="Me llamo:"
										autoCompleteType="name"
										autoCapitalize="words"
										style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
										onChangeText={(text) => { this.setState({name: text})}}
										value={this.state.name}
									/>
								</View>
								<View style={{margin: 5}}>
									<TextInput
										placeholder="Mi correo es:"
										autoCompleteType="email"
										keyboardType="email-address"
										style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
										onChangeText={(text) => { this.setState({email: text})}}
										value={this.state.email}
									/>
								</View>
								<View style={{margin: 5}}>
									<TextInput
										placeholder="Mi contraseña será:"
										secureTextEntry={true}
										style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
										onChangeText={(text) => { this.setState({password: text})}}
										value={this.state.password}
									/>
								</View>
								<View style={{margin: 5}}>
									<TextInput
										placeholder="Confirmo mi contraseña:"
										secureTextEntry={true}
										style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
										onChangeText={(text) => { this.setState({password_confirm: text})}}
										value={this.state.password_confirm}
									/>
								</View>
								<View style={{margin: 50, alignItems: 'center', justifyContent: 'center'}}>
									<Button title="Todo listo" onPress={this.registerUser}/>
								</View>
							</View>
						}
						{this.state.viewMode == 1 &&
							<View style={{padding: 10, width: '100%'}}>
								<View style={{margin: 5}}>
									<TextInput
										placeholder="Mi correo es:"
										autoCompleteType="email"
										keyboardType="email-address"
										style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
										onChangeText={(text) => { this.setState({email: text})}}
										value={this.state.email}
									/>
								</View>
								<View style={{margin: 5}}>
									<TextInput
										placeholder="Mi contraseña es:"
										secureTextEntry={true}
										onChangeText={(text) => { this.setState({password: text})}}
										value={this.state.password}
										style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
									/>
								</View>
								<View style={{margin: 50, alignItems: 'center', justifyContent: 'center'}}>
									<Button title="Continuar" onPress={this.signInUser}/>
								</View>
							</View>
						}
					</ScrollView>
				</Modal>
				<View style={{height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
					<Image source={require('../assets/pictures/bird.png')} style={{width: '50%', height: Layout.window.width/2, margin: 10}}/>
					<Text>Biomedical Issue Reporting Digital System</Text>
					<Text style={{fontWeight: 'bold'}}>B.I.R.D.S.</Text>
				</View>
				<View style={{width: '50%', height: (Layout.window.height/6), position: 'absolute', bottom: 0, alignItems: 'center', justifyContent: 'center'}}>
					<Button title="Acceder" onPress={()=>{this.setState({modalVisible: true})}}/>
				</View>
			</View>
		);
	}
}