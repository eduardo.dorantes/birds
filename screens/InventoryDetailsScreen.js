import React from 'react';
import { View, Text, SafeAreaView, Image, Button, ScrollView, TouchableOpacity, PixelRatio, Alert, Modal } from 'react-native';
import Icon from '../components/Icon';
import Layout from '../constants/Layout';
import isAdmin from '../constants/Admin';
import InventoryStatus from '../constants/InventoryStatus';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import ImageViewer from 'react-native-image-zoom-viewer';

export default class InventoryDetailsScreen extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			key:			'',
			nombre: 		'',
			serie: 			'',
			foto:			'',
			estado:			0,
			cantidad:		0,
			marca:			'',
			modelo:			'',
			foto_serie:		'',
			area:			'',
			inUse:			'',
			ready:			false,
			modalVisible:	false
		}
		this.getData			= this.getData.bind(this);
		this.deleteSnapshot		= this.deleteSnapshot.bind(this);
		this.editSnapshot		= this.editSnapshot.bind(this);
	}
	
	componentDidMount(){
		this.getData();
	}
	
	getData = async () => {
		const self				= this;
		const id				= this.props.navigation.state.params.id;
		const itemKey			= ("Inventory-" + id);
		const dataString		= await AsyncStorage.getItem(itemKey);
		if(dataString){
			const data = JSON.parse(dataString);
			self.setState({
				key:		data.key,
				nombre: 	data.nombre,
				serie: 		data.serie,
				foto:		data.foto,
				estado:		data.estado,
				area:		data.area,
				inUse:		data.inUse,
				cantidad:	data.cantidad,
				marca:		data.marca,
				modelo:		data.modelo,
				foto_serie:	data.foto_serie,
				ready:		true,
			});
		}
	}
	
	deleteSnapshot = async() => {
		if(!isAdmin(firebase.auth().currentUser.email)){
			Alert.alert("No se puede eliminar", "Sólo el administrador puede eliminar un registro.");
			return false;
		}
		const id				= this.props.navigation.state.params.id;
		const itemKey			= ("Inventory-" + id);
		const reference			= firebase.firestore().collection("inventario").where("key", "==", id)
		const navigation		= this.props.navigation;
		Alert.alert("Eliminar", "¿Desear eliminar este registro?",
			[
				{
					text: 'Cancelar',
					onPress: () => {},
					style: 'cancel',
				},
				{text: 'Sí', onPress: () => {
					reference
					.get()
					.then(function(querySnapshot){
						if(querySnapshot.docs.length > 0){
							const doc = querySnapshot.docs[0];
							doc.ref.delete()
							.then(function(){
								AsyncStorage.removeItem(itemKey)
								.then(()=>{
									navigation.goBack();
								})
								.catch(function(error){
									
								})
							})
							.catch(function(error){
								
							})
						}
					})
					.catch(function(error){
						
					});
				}},
			],
			{cancelable: true},
		)
	}
	
	editSnapshot = () => {
		const data	= this.state;
		const navigation = this.props.navigation;
		navigation.navigate("InventoryEdit",
			{
				data:{
					key:			data.key,
					nombre: 		data.nombre,
					serie: 			data.serie,
					foto:			data.foto,
					estado:			data.estado,
					area:			data.area,
					cantidad:		data.cantidad,
					marca:			data.marca,
					modelo:			data.modelo,
					foto_serie:		data.foto_serie,
					inUse:			data.inUse,
				},
				onGoBack:		this.getData
			}
		);
	}
	
	render(){
		const btnDisabled	=	true;
		const navigation	=	this.props.navigation;
		const tieneFoto		=	this.state.foto_serie != '' && this.state.foto_serie != null;
		var images = [
			{
				url: this.state.foto,
				props: {}
			}
		];
		if(tieneFoto){
			images.push({
				url:	this.state.foto_serie,
				props:	{}
			});
		}
		const isAdminUser = isAdmin(firebase.auth().currentUser.email);
		return(
			<SafeAreaView style={{width: '100%', height: '100%'}}>
				<Modal
					animationType="slide"
					visible={this.state.modalVisible}
					transparent={true}
					onRequestClose={
						() => {this.setState({modalVisible: false})}
					}
				>
					<View style={{width: '100%', height: 50, backgroundColor: 'transparent', position: 'absolute', zIndex: 2, justifyContent: 'center', paddingLeft: 5}}>
						<Icon Borderless name="arrow_left" onPress={() => {this.setState({modalVisible: false})}}/>
					</View>
					<ImageViewer imageUrls={images}/>
				</Modal>
				<View style={{width: '100%', height: 50, flexDirection: 'row', alignItems: 'center', padding: 5, backgroundColor: 'white', elevation: 1}}>
					<Icon Borderless name="arrow_left" onPress={() => {navigation.goBack()}}/>
					<Text style={{marginLeft: 10}}>Detalles</Text>
					{isAdminUser &&
						<View style={{position: 'absolute', right: 10, backgroundColor: 'white', width: 100, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>					
							<TouchableOpacity onPress={this.editSnapshot} style={{height: 20, width: 50, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
								<Icon Borderless name="pencil" forceColor color="black"/>
							</TouchableOpacity>
							<TouchableOpacity onPress={this.deleteSnapshot} style={{height: 20, width: 50, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
								<Icon Borderless name="trash" forceColor color="black"/>
							</TouchableOpacity>
						</View>
					}
				</View>
				<ScrollView style={{width: '100%', height: Layout.window.height - 50}}>
					{(this.state.foto != "" && this.state.foto != null) &&
						<TouchableOpacity onPress={() => {this.setState({modalVisible: true})}} style={{width: Layout.window.width, height: Layout.window.width, backgroundColor: 'black'}}>
							<Image source={{uri: this.state.foto}} style={{width: '100%', height: '100%', resizeMode: 'contain'}}/>
						</TouchableOpacity>
					}
					<Text style={{textAlign: 'center', fontWeight: 'bold'}}>{tieneFoto ? 'La fotografía ' + (images.length) + ' corresponde al no. de serie.' : 'El documento no incluye fotografía del número de serie.'}</Text>
					<View style={{margin: 10}}>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Nombre</Text>
							<Text style={{}}>{this.state.nombre}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Número de serie</Text>
							<Text style={{}}>{this.state.serie}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Cantidad</Text>
							<Text style={{}}>{this.state.cantidad}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Marca</Text>
							<Text style={{}}>{this.state.marca}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Modelo</Text>
							<Text style={{}}>{this.state.modelo}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Área</Text>
							<Text style={{}}>{this.state.area}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Estado</Text>
							<Text style={{color: InventoryStatus[this.state.estado].color}}>{InventoryStatus[this.state.estado].label}</Text>
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}