import React from 'react';
import { View, Text, ScrollView, SafeAreaView, TextInput, Image, TouchableOpacity, Button, PixelRatio, Modal } from 'react-native';
import Icon from '../components/Icon';
import ImagePicker from 'react-native-image-picker';
import uuid from 'uuid/v4';
import firebase from 'react-native-firebase';
const ImageReady = require('../assets/pictures/complete.png');
import { uploadFile } from '../helpers';

const options = {
	title: 'Selecciona una imagen',
	takePhotoButtonTitle: 'Desde cámara',
	chooseFromLibraryButtonTitle: 'De mi galería',
	maxWidth: 1280,
	maxHeight: 720,
	mediaType: 'photo',
	storageOptions: {
		skipBackup: true,
		path: 'images'
	}
};

export default class IssueNewScreen extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			title:			'',
			issue:			'',
			solution:		'',
			photo:			'',
			status:			false,
			modalVisible:	false,
			uploading:		false,
			uploadProgress:	0,
			uploadComplete: false,
			saveComplete:	false
		};
		this.database			= firebase.firestore().collection("reportes");
		this.addIssue			= this.addIssue.bind(this);
	}
	
	componentDidMount(){
		
	}
	
	componentWillUnmount(){
		
	}
	
	pickImage = () => {
		ImagePicker.showImagePicker(options, response => {
			if (response.didCancel) {
				
			}
			else if (response.error) {
				
			}
			else {
				const source = { uri: response.uri };
				this.setState({
					imgSource: source,
					photo: response.uri
				});
			}
		});
	};
	
	addIssue = () => {
		const self = this;
		this.setState({modalVisible: true});
		if(this.state.photo != ""){
			uploadFile(
				'images',
				this.state.photo,
				(url) => {
					self.database.add({
						key:		`${uuid()}`,
						title: 		self.state.title.trim(),
						issue: 		self.state.issue.trim(),
						solution: 	self.state.solution.trim(),
						status: 	self.state.status,
						photo:		url,
						author:		firebase.auth().currentUser.uid,
						date:		new Date().getTime()
					});
					self.setState({
						uploading: 		false,
						uploadProgress: 0,
						uploadComplete: true,
						modalVisible: 	false
					});
					self.props.navigation.goBack();
				},
				(error) => {
					self.setState({
						uploading: 		false,
						uploadProgress: 0,
						uploadComplete: false,
						modalVisible: 	false
					});
				},
				(percent) => {
					self.setState({uploadProgress: percent});
				}
			);
		}
		else{
			this.database.add({
				key:		`${uuid()}`,
				title: 		this.state.title.trim(),
				issue: 		this.state.issue.trim(),
				solution: 	this.state.solution.trim(),
				status: 	this.state.status,
				photo:		null,
				author:		firebase.auth().currentUser.uid,
				date:		new Date().getTime()
			});
			setTimeout(()=>{				
				self.setState({modalVisible: false});
				self.props.navigation.goBack();
			}, 1500);
		}
	}
	
	render() {
		const navigation	= this.props.navigation;
		const btnDisabled	= !(this.state.title.trim() != "" && this.state.issue.trim() != "");
		return (
			<SafeAreaView style={{ width: '100%', height: '100%'}}>
				<Modal
					animationType="slide"
					transparent={false}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						
					}}
				>
					<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
						<Image source={ImageReady} style={{width: '50%', resizeMode: 'contain'}}/>						
						<View style={{width: '100%', height: 50, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
							<Text>{"Guardando..." + " " + this.state.uploadProgress + "%"}</Text>
						</View>
					</View>
				</Modal>
				<View style={{width: '100%', height: 50, flexDirection: 'row', alignItems: 'center', padding: 5, backgroundColor: 'white', elevation: 1}}>
					<Icon Borderless name="arrow_left" onPress={() => {navigation.goBack()}}/>
					<Text style={{marginLeft: 10}}> Crear reporte</Text>
					<View style={{alignItems: 'center', position: 'absolute', right: 10}}>
						<Button title="Guardar" disabled={btnDisabled} onPress={this.addIssue}/>
					</View>
				</View>
				<ScrollView contentContainerStyle={{padding: 10}}>
					<View>
						<Text>Crearemos el reporte de la incidencia.{"\n"}Por favor, completa los siguientes campos:</Text>
					</View>
					<React.Fragment>
						<TextInput
							placeholder="Título"
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({title: text})}}
							value={this.state.title}
						/>
					</React.Fragment>
					<React.Fragment>
						<TextInput
							placeholder="Problema"
							multiline={true}
							numberOfLines={4}
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({issue: text})}}
							value={this.state.issue}
						/>
					</React.Fragment>
					<React.Fragment>
						<TextInput
							placeholder="Solución"
							multiline={true}
							numberOfLines={4}
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({solution: text})}}
							value={this.state.solution}
						/>
					</React.Fragment>
					<View
						style={{height: '40%', margin: 20, marginTop: 50, alignItems: 'center'}}
					>
						<View
							style={{
								width: 300,
								height: 300,
								backgroundColor: (this.state.photo == "" ? '#EEEEEE' : 'transparent'),
								alignItems: 'center', 
								justifyContent: 'center'
							}}
						>
							{this.state.photo != "" &&
								<Image source={{uri: this.state.photo}} style={{width: 300, height: 300, resizeMode: 'contain'}}/>
							}
							{this.state.photo == "" &&
								<View>
									<Text>Adjunta una fotografía al reporte</Text>
								</View>
							}
						</View>
						<View style={{marginTop: 20, borderBottomWidth: 1/PixelRatio.get(), borderBottomColor: 'rgba(200, 200, 200, 0.75)', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', height: 50}}>
							<TouchableOpacity
								onPress={()=>{this.setState({photo: ''})}}
								style={{borderWidth: 1, borderRadius: 10, borderColor: 'gray', padding: 5, width: '30%', }}
							>
								<Text style={{textAlign: 'center'}}>Eliminar</Text>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={this.pickImage}
								style={{borderWidth: 1, borderRadius: 10, borderColor: 'gray', padding: 5, width: '30%'}}
							>
								<Text style={{textAlign: 'center'}}>Seleccionar</Text>
							</TouchableOpacity>
						</View>
						<View style={{width: '100%', height: 50, flexDirection: 'row', marginTop: 20}}>
							<TouchableOpacity
								onPress={() => { this.setState({status: false}) }}
								style={{
									width: '50%',
									height: 50,
									backgroundColor: (!this.state.status ? 'gold' : 'transparent'),
									borderBottomLeftRadius: 10,
									borderTopLeftRadius: 10,
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<Text
									style={{
										textAlign: 'center',
										color: (!this.state.status ? 'white' : 'black')
									}}
								>Pendiente</Text>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => { this.setState({status: true}) }}
								style={{
									width: '50%',
									height: 50,
									backgroundColor: (this.state.status ? 'green' : 'transparent'),
									borderBottomRightRadius: 10,
									borderTopRightRadius: 10,
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<Text
									style={{
										textAlign: 'center',
										color: (this.state.status ? 'white' : 'black')
									}}
								>Solucionado</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}