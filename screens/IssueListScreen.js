import React from 'react';
import { View, Text, FlatList, Modal, Image, TouchableOpacity, Button, Alert, TextInput } from 'react-native';
import firebase from 'react-native-firebase';
import Header from '../components/Header';
import Icon from '../components/Icon';
import Layout from '../constants/Layout';
import AsyncStorage from '@react-native-community/async-storage';
import isAdmin from '../constants/Admin';

const EmptyBox = require('../assets/pictures/empty.png');

export default class IssueListScreen extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			data:			[],
			filtered_data:	[],
			filter:			''
		}
		this.unsubscribe		= null;
		this.onCollectionUpdate = this.onCollectionUpdate.bind(this);
		this.filterAndSetData	= this.filterAndSetData.bind(this);
		this.database = firebase.firestore().collection("reportes");
	}
	
	componentDidMount(){
		this.unsubscribe = this.database.onSnapshot(this.onCollectionUpdate);
	}
	
	componentWillUnmount(){
		this.unsubscribe();
	}
	
	onCollectionUpdate = (querySnapshot) => {
		var data	= [];
		querySnapshot.forEach((doc) => {
			const docData	= doc.data();
			var document	= {
				key:		docData.key,
				title:		docData.title,
				issue:		docData.issue,
				solution:	docData.solution,
				photo:		docData.photo,
				date:		docData.date,
				status:		docData.status,
				author:		docData.author
			};
			
			const documentKey	= ("Issue-" + document.key);
			AsyncStorage.setItem(documentKey, JSON.stringify(document));
			data.push(document);
		});
		
		data = data.sort((a, b) => {
			return a.date > b.date
		});
		this.setState({data: data});
		this.filterAndSetData();
	}
	
	filterAndSetData = () => {
		const data		= this.state.data;
		const filter	= this.state.filter.trim().toLowerCase();
		var dataToSet	= [];
		if(filter != ''){
			dataToSet = data.map((item) => {
				if((item.title.toLowerCase().indexOf(filter) > -1) || (item.issue.toLowerCase().indexOf(filter) > -1) || (item.solution.toLowerCase().indexOf(filter) > -1)){
					return item;
				}
			});
			dataToSet = dataToSet.filter((obj) => obj);
		}
		else{
			dataToSet = data;
		}
		this.setState({filtered_data: dataToSet});
	}
	
	render() {
		const navigation = this.props.navigation;
		return (
			<View style={{ width: '100%', height: '100%', backgroundColor: 'white'}}>
				<Header/>
				<View style={{width: '100%', backgroundColor: '#FFFFFF', borderBottomWidth: 1, borderBottomColor: '#CCCCCC', flexDirection: 'row', justifyContent: 'space-between'}}>
					<View style={{margin: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
						{/*<Text>{this.state.data.length + " reportes"}</Text>*/}
						<TextInput
							placeholder="Filtro"
							style={{padding: 0, borderBottomWidth: 1, borderBottomColor: 'gray', width: 150}}
							multiline={false}
							onChangeText={(text) => {
								this.setState({filter: text});
								this.filterAndSetData();
							}}
							value={this.state.filter}
						/>
						<Icon Borderless size={25} factor={1} name="trash" onPress={() => {
							this.setState({filter: ''});
							this.filterAndSetData();
						}}/>
					</View>
					<TouchableOpacity
						onPress={() => {
							if(!firebase.auth().currentUser.emailVerified){
								Alert.alert(
									"No se ha activado tu cuenta",
									"Solicita al administrador que realice el proceso.",
									[
										{
											text: '¡Entendido!',
											onPress: () => {},
										}
									],
									{cancelable: true}
								);
							}
							else{
								navigation.navigate("IssueNew");
							}
						}}
						style={{margin: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}
					>
						<Text style={{marginLeft: 5}}>Nuevo Reporte</Text>
					</TouchableOpacity>
				</View>
				{this.state.filtered_data.length == 0 &&
					<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
						<Image source={EmptyBox} style={{width: '50%', resizeMode: 'contain'}}/>
						<Text>No hay nada por aquí...</Text>
						<Text>Intenta agregar un nuevo registro.</Text>
					</View>
				}
				{this.state.filtered_data.length > 0 &&
					<FlatList
						data={this.state.filtered_data}
						renderItem={({item}) => {
							return(
								<TouchableOpacity onPress={() => navigation.navigate("IssueDetail", {id: item.key})} style={{flexDirection: 'row', width: '100%', height: 50, alignItems: 'center'}}>
									<View style={{width: 40, height: 40, marginLeft: 10, marginRight: 10, borderRadius: 10, backgroundColor: '#EEEEEE'}}>
										{item.photo != "" &&
											<Image source={{uri: item.photo}} style={{width: 40, height: 40, resizeMode: 'cover'}}/>
										}
									</View>
									<View style={{width: Layout.window.width - 70 - 70}}>
										<Text style={{fontWeight: 'bold'}}>{item.title}</Text>
										<Text style={{}}>{item.issue.length > 25 ? item.issue.substr(0, 25) + "..." : item.issue}</Text>
									</View>
									<View style={{width: 60, height: 25, alignItems: 'center', justifyContent: 'center', borderRadius: 5, backgroundColor: (item.status ? 'green' : 'gold')}}>
										<Text style={{fontSize: 10, textAlign: 'center', color: 'white'}}>{item.status ? "Solucionado" : "Pendiente"}</Text>
									</View>
								</TouchableOpacity>
							)
						}}
					/>
				}
			</View>
		);
	}
}