import React from 'react';
import { View, Text, SafeAreaView, Image, Button, ScrollView, TouchableOpacity, PixelRatio, Alert, Modal } from 'react-native';
import Icon from '../components/Icon';
import Layout from '../constants/Layout';
import isAdmin from '../constants/Admin';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import ImageViewer from 'react-native-image-zoom-viewer';

export default class IssueDetailScreen extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			key:			'',
			title:			'',
			issue:			'',
			solution:		'',
			photo:			'',
			status:			false,
			ready:			false,
			modalVisible:	false
		}
		this.getData			= this.getData.bind(this);
		this.deleteSnapshot		= this.deleteSnapshot.bind(this);
		this.editSnapshot		= this.editSnapshot.bind(this);
	}
	
	componentDidMount(){
		this.getData();
	}
	
	getData = async () => {
		const self				= this;
		const id				= this.props.navigation.state.params.id;
		const itemKey			= ("Issue-" + id);
		const dataString		= await AsyncStorage.getItem(itemKey);
		if(dataString){
			const data = JSON.parse(dataString);
			self.setState({
				key:		data.key,
				title: 		data.title,
				issue: 		data.issue,
				solution:	data.solution,
				photo:		data.photo,
				date:		data.date,
				author:		data.author,
				status:		data.status,
				ready:		true,
			});
		}
	}
	
	deleteSnapshot = async() => {
		if(!isAdmin(firebase.auth().currentUser.email)){
			Alert.alert("No se puede eliminar", "Sólo el administrador puede eliminar un reporte.");
			return false;
		}
		const id				= this.props.navigation.state.params.id;
		const itemKey			= ("Issue-" + id);
		const reference			= firebase.firestore().collection("reportes").where("key", "==", id)
		const navigation		= this.props.navigation;
		Alert.alert("Eliminar", "¿Desear eliminar este reporte?",
			[
				{
					text: 'Cancelar',
					onPress: () => {},
					style: 'cancel',
				},
				{text: 'Sí', onPress: () => {
					reference
					.get()
					.then(function(querySnapshot){
						if(querySnapshot.docs.length > 0){
							const doc = querySnapshot.docs[0];
							doc.ref.delete()
							.then(function(){
								AsyncStorage.removeItem(itemKey)
								.then(()=>{
									navigation.goBack();
								})
								.catch(function(error){
									
								})
							})
							.catch(function(error){
								
							})
						}
					})
					.catch(function(error){
						
					});
				}},
			],
			{cancelable: true},
		)
	}
	
	editSnapshot = () => {
		const state	= this.state;
		const navigation = this.props.navigation;
		navigation.navigate("IssueEdit",
			{
				data:{
					key:		state.key,
					title: 		state.title,
					issue: 		state.issue,
					solution:	state.solution,
					photo:		state.photo,
					status:		state.status,
					author:		state.author,
					date:		state.date
				},
				onGoBack:		this.getData
			}
		);
	}
	
	render(){
		const btnDisabled = true;
		const navigation = this.props.navigation;
		const images = [
			{
				url: this.state.photo,
				props: {}
			}
		];
		const isAdminUser = isAdmin(firebase.auth().currentUser.email);
		return(
			<SafeAreaView style={{width: '100%', height: '100%'}}>
				<Modal
					animationType="slide"
					visible={this.state.modalVisible}
					transparent={true}
					onRequestClose={
						() => {this.setState({modalVisible: false})}
					}
				>
					<View style={{width: '100%', height: 50, backgroundColor: 'transparent', position: 'absolute', zIndex: 2, justifyContent: 'center', paddingLeft: 5}}>
						<Icon Borderless name="arrow_left" onPress={() => {this.setState({modalVisible: false})}}/>
					</View>
					<ImageViewer imageUrls={images}/>
				</Modal>
				<View style={{width: '100%', height: 50, flexDirection: 'row', alignItems: 'center', padding: 5, backgroundColor: 'white', elevation: 1}}>
					<Icon Borderless name="arrow_left" onPress={() => {navigation.goBack()}}/>
					<Text style={{marginLeft: 10}}>Detalles</Text>
					<View style={{position: 'absolute', right: 10, backgroundColor: 'white', width: (isAdminUser ? 100 : 50), flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>					
						<TouchableOpacity onPress={this.editSnapshot} style={{height: 20, width: 50, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
							<Icon Borderless name="pencil" forceColor color="black"/>
						</TouchableOpacity>
						{isAdminUser &&
							<TouchableOpacity onPress={this.deleteSnapshot} style={{height: 20, width: 50, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
								<Icon Borderless name="trash" forceColor color="black"/>
							</TouchableOpacity>
						}
					</View>
				</View>
				<ScrollView style={{width: '100%', height: Layout.window.height - 50}}>
					{(this.state.photo != "" && this.state.photo != null) &&
						<TouchableOpacity onPress={() => {this.setState({modalVisible: true})}} style={{width: Layout.window.width, height: Layout.window.width, backgroundColor: 'black'}}>
							<Image source={{uri: this.state.photo}} style={{width: '100%', height: '100%', resizeMode: 'contain'}}/>
						</TouchableOpacity>
					}
					<View style={{margin: 10}}>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Título</Text>
							<Text style={{}}>{this.state.title}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Problema</Text>
							<Text style={{}}>{this.state.issue}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Solución</Text>
							<Text style={{}}>{this.state.solution.trim() != "" ? this.state.solution : "-- --"}</Text>
						</View>
						<View style={{marginBottom: 10}}>
							<Text style={{fontWeight: 'bold'}}>Estado</Text>
							<Text style={{color: (this.state.status ? 'green' : 'gold')}}>{this.state.status ? "Solucionado" : "Pendiente"}</Text>
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}