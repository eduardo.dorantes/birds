import React from 'react';
import { View, SafeAreaView, ScrollView, Image, Text, TouchableOpacity, Modal, FlatList, TextInput } from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../components/Header';
import Icon from '../components/Icon';
import Layout from '../constants/Layout';
import isAdmin from '../constants/Admin';
import InventoryItem from '../components/InventoryItem';
const EmptyBox = require('../assets/pictures/empty.png');

export default class AdminMainScreen extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			viewActive: 		0,
			inventory:			[],
			users:				[],
			refreshing: 		false,
			filtered_inventory:	[],
			filter:				''
		}
		this.usersView			= this.usersView.bind(this);
		this.inventoryView		= this.inventoryView.bind(this);
		this.fetchUsers			= this.fetchUsers.bind(this);
		this.switchUserVerification	= this.switchUserVerification.bind(this);
		
		this.unsubscribe_inventory		= null;
		this.inventory					= firebase.firestore().collection("inventario");
		this.onInventoryUpdate			= this.onInventoryUpdate.bind(this);
		this.filterAndSetData			= this.filterAndSetData.bind(this);
	}
	
	componentDidMount(){
		this.unsubscribe_inventory = this.inventory.onSnapshot(this.onInventoryUpdate);
		this.fetchUsers();
	}
	
	componentWillUnmount(){
		this.unsubscribe();
	}
	
	onInventoryUpdate = (querySnapshot) => {
		var data = [];
		querySnapshot.forEach((doc) => {
			const docData	= doc.data();
			const documentKey	= ("Inventory-" + docData.key);
			AsyncStorage.setItem(documentKey, JSON.stringify(docData));
			data.push({...docData, ref: doc});
		});
		
		this.setState({
			inventory: data
		});
		
		this.filterAndSetData();
	}
	
	filterAndSetData = () => {
		const data		= this.state.inventory;
		const filter	= this.state.filter.trim().toLowerCase();
		var dataToSet	= [];
		if(filter != '' && this.state.viewActive == 1){
			dataToSet = data.map((item) => {
				if(
					(item.nombre.toLowerCase().indexOf(filter) > -1) ||
					(item.marca.toLowerCase().indexOf(filter) > -1) ||
					(item.modelo.toLowerCase().indexOf(filter) > -1) ||
					(item.serie.toLowerCase().indexOf(filter) > -1) ||
					(item.estado == 0 && filter == "ok") ||
					(item.estado == 1 && ["reparacion", "reparación"].includes(filter)) ||
					(item.estado == 2 && filter == "dañado")
				){
					return item;
				}
			});
			dataToSet = dataToSet.filter((obj) => obj);
		}
		else{
			dataToSet = data;
		}
		this.setState({filtered_inventory: dataToSet});
	}
	
	fetchUsers = () => {
		const self = this;
		this.setState({refreshing: true});
		const getUsersData = firebase.functions().httpsCallable("inAppgetAllUsers");
		getUsersData({})
		.then(function(data){
			self.setState({users: data.data, refreshing: false});
		})
		.catch(function(error){
			self.setState({refreshing: false});
		});
	}
	
	switchUserVerification = (uid, value) => {
		const self = this;
		this.setState({refreshing: true});
		const setActive = firebase.functions().httpsCallable("inAppActivateUser");
		const setInactive = firebase.functions().httpsCallable("inAppDeactivateUser");
		
		if(value){
			setActive({uid: uid})
			.then(function(response){
				self.fetchUsers();
			})
			.catch(function(error){
				self.fetchUsers();
			});
		}
		else{
			setInactive({uid: uid})
			.then(function(response){
				self.fetchUsers();
			})
			.catch(function(error){
				self.fetchUsers();
			});
		}
	}
	
	usersView = () => {
		if(this.state.users.length == 2){
			return(
				<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
					<Image source={EmptyBox} style={{width: '50%', resizeMode: 'contain'}}/>
					<Text>No hay nada por aquí...</Text>
					<Text>Intenta agregar un nuevo registro.</Text>
				</View>
			);
		}
		else{
			return (
				<View style={{width: '100%', height: Layout.window.height - 100}}>
					<FlatList
						onRefresh={this.fetchUsers}
						refreshing={this.state.refreshing}
						data={this.state.users}
						keyExtractor={(item, index) => item.uid}
						renderItem={({item}) => {
							if(isAdmin(item.email)){
								return null;
							}
							
							return(
								<View style={{width: Layout.window.width, height: 50, flexDirection: 'row'}}>
									<View style={{width: Layout.window.width - 60}}>
										<Text numberOfLines={1} style={{fontWeight: 'bold'}}>{item.displayName}</Text>
										<Text numberOfLines={1}>{item.email}</Text>
									</View>
									<View style={{width: 60, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
										<TouchableOpacity onPress={() => {this.switchUserVerification(item.uid, !item.emailVerified)}} style={{width: 50, height: 20, backgroundColor: (item.emailVerified ? 'green' : 'orange'), borderRadius: 10, alignItems: 'center', justifyContent: 'center'}}>
											<Text style={{fontSize: 10, textAlign: 'center', color: 'white'}}>{item.emailVerified ? 'Activo' : 'Inactivo'}</Text>
										</TouchableOpacity>
									</View>
								</View>
							);
						}}
					/>
				</View>
			);
		}
	}
	
	inventoryView = () => {
		const navigation = this.props.navigation;
		if(this.state.inventory.length == 0){
			return(
				<View style={{}}>
					<View style={{height: 30, flexDirection: 'row', alignItems: 'center', elevation: 1, backgroundColor: '#EEEEEE'}}>
						<TouchableOpacity onPress={() => navigation.navigate("InventoryAdd")} style={{width: 75, position: 'absolute', right: 10}}>
							<Text>Agregar</Text>
						</TouchableOpacity>
					</View>
					<View style={{width: '100%', height: (Layout.window.height - 30), alignItems: 'center', justifyContent: 'center'}}>
						<Image source={EmptyBox} style={{width: '50%', resizeMode: 'contain'}}/>
						<Text>No hay nada por aquí...</Text>
						<Text>Intenta agregar un nuevo registro.</Text>
					</View>
				</View>
			);
		}
		else{
			return (
				<View style={{}}>
					<View style={{height: 40, flexDirection: 'row', alignItems: 'center', elevation: 1, backgroundColor: '#EEEEEE'}}>
						<View style={{margin: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
							<TextInput
								placeholder="Filtro"
								style={{padding: 0, borderBottomWidth: 1, borderBottomColor: 'gray', width: 150}}
								multiline={false}
								onChangeText={(text) => {
									this.setState({filter: text});
									this.filterAndSetData();
								}}
								value={this.state.filter}
							/>
							<Icon Borderless size={25} factor={1} name="trash" onPress={() => {
								this.setState({filter: ''});
								this.filterAndSetData();
							}}/>
						</View>
						<TouchableOpacity onPress={() => navigation.navigate("InventoryAdd")} style={{width: 75, position: 'absolute', right: 10}}>
							<Text>Agregar</Text>
						</TouchableOpacity>
					</View>
					<View style={{width: '100%', height: Layout.window.height - 170}}>
						<FlatList
							data={this.state.filtered_inventory}
							renderItem={({item}) => {
								return(
									<InventoryItem data={item} onPress={() => {navigation.navigate("InventoryDetails", {id: item.key})}}/>
								)
							}}
						/>
					</View>
				</View>
			);
		}
	}
	
	render(){
		const menuOptions	=	[
			{key: 0, name: "Usuarios",		icon: '', command: () => {this.setState({viewActive: 0})}},
			{key: 1, name: "Inventario",	icon: '', command: () => {this.setState({viewActive: 1})}},
		];
		
		const menuItems		=	menuOptions.map((item) => {
			return(
				<TouchableOpacity
					key={item.key}
					style={{
						padding: 5,
						alignItems: 'center',
						justifyContent: 'center',
						width: (Layout.window.width/menuOptions.length),
						backgroundColor: (this.state.viewActive == item.key ? '#EEEEEE' : 'white'),
					}}
					onPress={item.command}
				>
					<Text style={{
						textAlign: 'center'
					}}>{item.name}</Text>
				</TouchableOpacity>
			);
		});
		
		if(isAdmin(firebase.auth().currentUser.email)){
			return(
				<SafeAreaView style={{backgroundColor: 'white'}}>
					<Header/>
					<View style={{width: '100%', flexDirection: 'row', backgroundColor: 'white', elevation: 1}}>
						{menuItems}
					</View>
					{this.state.viewActive == 0 && this.usersView()}
					{this.state.viewActive == 1 && this.inventoryView()}
				</SafeAreaView>
			);
		}
		else{
			return(
				<SafeAreaView>
					<Header/>
					<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
						<Text>No tienes permisos para acceder a esta vista.</Text>
					</View>
				</SafeAreaView>
			);
		}
	}
}