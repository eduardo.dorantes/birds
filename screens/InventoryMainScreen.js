import React from 'react';
import { View, SafeAreaView, ScrollView, Image, Text, TouchableOpacity, Modal, FlatList, TextInput } from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../components/Header';
import Icon from '../components/Icon';
import Layout from '../constants/Layout';
import InventoryItem from '../components/InventoryItem';
const EmptyBox = require('../assets/pictures/empty.png');

export default class InventoryMainScreen extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			viewActive: 0,
			inventory:				[],
			inventoryAvailable:		[],
			inventoryInUse:			[],
			filter:					'',
			filtered_inventory:		[]
		}
		this.renderAvailable			= this.renderAvailable.bind(this);
		this.renderInUse				= this.renderInUse.bind(this);
		this.renderAll					= this.renderAll.bind(this);
		this.unsubscribe_inventory		= null;
		this.inventory					= firebase.firestore().collection("inventario");
		this.onInventoryUpdate			= this.onInventoryUpdate.bind(this);
		this.filterAndSetData			= this.filterAndSetData.bind(this);
	}
	
	componentDidMount(){
		this.unsubscribe_inventory = this.inventory.onSnapshot(this.onInventoryUpdate);
	}
	
	componentWillUnmount(){
		this.unsubscribe();
	}
	
	onInventoryUpdate = (querySnapshot) => {
		var available	= [];
		var inUse		= [];
		var general		= [];
		querySnapshot.forEach((doc) => {
			const docData	= doc.data();
			const documentKey	= ("Inventory-" + docData.key);
			AsyncStorage.setItem(documentKey, JSON.stringify(docData));
			general.push(docData);
			if(docData.inUse){
				inUse.push(docData);
			}
			else{
				available.push(docData);
			}
		});
		
		this.setState({
			inventoryAvailable: available,
			inventoryInUse:		inUse,
			inventory:			general
		});
		
		this.filterAndSetData();
	}
	
	filterAndSetData = () => {
		const data		= this.state.inventory;
		const filter	= this.state.filter.trim().toLowerCase();
		var dataToSet	= [];
		if(filter != '' && this.state.viewActive == 0){
			dataToSet = data.map((item) => {
				if(
					(item.nombre.toLowerCase().indexOf(filter) > -1) ||
					(item.marca.toLowerCase().indexOf(filter) > -1) ||
					(item.modelo.toLowerCase().indexOf(filter) > -1) ||
					(item.serie.toLowerCase().indexOf(filter) > -1) ||
					(item.estado == 0 && filter == "ok") ||
					(item.estado == 1 && ["reparacion", "reparación"].includes(filter)) ||
					(item.estado == 2 && filter == "dañado")
				){
					return item;
				}
			});
			dataToSet = dataToSet.filter((obj) => obj);
		}
		else{
			dataToSet = data;
		}
		this.setState({filtered_inventory: dataToSet});
	}
	
	
	renderAvailable = () => {
		const navigation = this.props.navigation;
		if(this.state.inventoryAvailable.length == 0){
			return(
				<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
					<Image source={EmptyBox} style={{width: '50%', resizeMode: 'contain'}}/>
					<Text>No hay nada por aquí...</Text>
					<Text>Intenta agregar un nuevo registro.</Text>
				</View>
			);
		}
		else{
			return(
				<FlatList
					data={this.state.inventoryAvailable}
					renderItem={({item}) => {
						return(
							<InventoryItem data={item} onPress={() => {navigation.navigate("InventoryDetails", {id: item.key})}}/>
						);
					}}
				/>
			);
		}
	}
	
	renderInUse = () => {
		const navigation = this.props.navigation;
		if(this.state.inventoryInUse.length == 0){
			return(
				<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
					<Image source={EmptyBox} style={{width: '50%', resizeMode: 'contain'}}/>
					<Text>No hay nada por aquí...</Text>
					<Text>Intenta agregar un nuevo registro.</Text>
				</View>
			);
		}
		else{
			return(
				<FlatList
					data={this.state.inventoryInUse}
					renderItem={({item}) => {
						return(
							<InventoryItem data={item} onPress={() => {navigation.navigate("InventoryDetails", {id: item.key})}}/>
						);
					}}
				/>
			);
		}
	}
	
	renderAll = () => {
		const navigation = this.props.navigation;
		if(this.state.inventory.length == 0){
			return(
				<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
					<Image source={EmptyBox} style={{width: '50%', resizeMode: 'contain'}}/>
					<Text>No hay nada por aquí...</Text>
					<Text>Intenta agregar un nuevo registro.</Text>
				</View>
			);
		}
		else{
			return(
				<FlatList
					data={this.state.filtered_inventory}
					renderItem={({item}) => {
						return(
							<InventoryItem data={item} onPress={() => {navigation.navigate("InventoryDetails", {id: item.key})}}/>
						);
					}}
				/>
			);
		}
	}
	
	render(){
		/*
		const menuOptions	=	[
			{key: 0, name: "Todo",				icon: '', command: () => {this.setState({viewActive: 0})}},
			{key: 1, name: "Disponible",		icon: '', command: () => {this.setState({viewActive: 1})}},
			{key: 2, name: "En uso",			icon: '', command: () => {this.setState({viewActive: 2})}},
		];
		
		const menuItems		=	menuOptions.map((item) => {
			return(
				<TouchableOpacity
					key={item.key}
					style={{
						padding: 5,
						alignItems: 'center',
						justifyContent: 'center',
						width: (Layout.window.width/menuOptions.length),
						backgroundColor: (this.state.viewActive == item.key ? '#EEEEEE' : 'white'),
					}}
					onPress={item.command}
				>
					<Text style={{
						textAlign: 'center'
					}}>{item.name}</Text>
				</TouchableOpacity>
			);
		});
		*/
		return(
			<SafeAreaView>
				<Header/>
				<View style={{width: '100%', backgroundColor: 'white', elevation: 1}}>
					<View style={{margin: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
						<TextInput
							placeholder="Filtro"
							style={{padding: 0, borderBottomWidth: 1, borderBottomColor: 'gray', width: 150}}
							multiline={false}
							onChangeText={(text) => {
								this.setState({filter: text});
								this.filterAndSetData();
							}}
							value={this.state.filter}
						/>
						<Icon Borderless size={25} factor={1} name="trash" onPress={() => {
							this.setState({filter: ''});
							this.filterAndSetData();
						}}/>
					</View>
				</View>
				<View style={{width: '100%', height: Layout.window.height - 140}}>
					{/*
					<View style={{width: '100%', flexDirection: 'row', backgroundColor: 'white', elevation: 1}}>
						{menuItems}
					</View>
					*/}
					{this.state.viewActive == 0 && this.renderAll()}
					{/*this.state.viewActive == 1 && this.renderAvailable()*/}
					{/*this.state.viewActive == 2 && this.renderInUse()*/}
				</View>
			</SafeAreaView>
		);
	}
}