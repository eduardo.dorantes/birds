import React from 'react';
import { View, Text, ScrollView, SafeAreaView, TextInput, Image, TouchableOpacity, Button, PixelRatio, Modal, BackHandler } from 'react-native';
import Icon from '../components/Icon';
import isAdmin from '../constants/Admin';
import InventoryStatus from '../constants/InventoryStatus';
import ImagePicker from 'react-native-image-picker';
import uuid from 'uuid/v4';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
const ImageReady = require('../assets/pictures/complete.png');
import { uploadFile } from '../helpers';

const options = {
	title: 'Selecciona una imagen',
	takePhotoButtonTitle: 'Desde cámara',
	chooseFromLibraryButtonTitle: 'De mi galería',
	maxWidth: 1280,
	maxHeight: 720,
	mediaType: 'photo',
	storageOptions: {
		skipBackup: true,
		path: 'images'
	}
};

export default class InventoryEditScreen extends React.Component {
	constructor(props){
		super(props);
		const data	=	this.props.navigation.state.params.data;
		this.state = {
			key:		data.key,
			nombre: 	data.nombre,
			serie: 		data.serie,
			foto:		data.foto,
			estado:		data.estado,
			area:		data.area,
			cantidad:	data.cantidad,
			marca:		data.marca,
			modelo:		data.modelo,
			foto_serie:	data.foto_serie,
			inUse:		data.inUse,
			modalVisible:	false,
			uploading:		false,
			uploadProgress:	0,
			uploadComplete: false,
			saveComplete:	false
		}
		this.database			= firebase.firestore().collection("inventario");
		this.addIssue			= this.addIssue.bind(this);
		this.saveIssue			= this.saveIssue.bind(this);
		this.uploadMainPhoto	= this.uploadMainPhoto.bind(this);
		this.uploadSeriesPhoto	= this.uploadSeriesPhoto.bind(this);
		this.handleBackPress	= this.handleBackPress.bind(this);
	}
	
	componentDidMount(){
		this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
	}
	
	componentWillUnmount(){
		this.backHandler.remove()
	}
	
	handleBackPress = () => {
		this.props.navigation.state.params.onGoBack();
		this.props.navigation.goBack();
		return true;
	}
	
	pickImage = () => {
		ImagePicker.showImagePicker(options, response => {
			if (response.didCancel) {
				
			}
			else if (response.error) {
				
			}
			else {
				const source = { uri: response.uri };
				this.setState({
					imgSource: source,
					foto: response.uri
				});
			}
		});
	};
	
	pickImageSeries = () => {
		ImagePicker.showImagePicker(options, response => {
			if (response.didCancel) {
				
			}
			else if (response.error) {
				
			}
			else {
				const source = { uri: response.uri };
				this.setState({
					imgSource: source,
					foto_serie: response.uri
				});
			}
		});
	};
	
	uploadMainPhoto = () => {
		const self = this;
		uploadFile(
			'images',
			this.state.foto,
			(url) => {
				self.setState({foto: url});
				self.setState({
					uploading: 		false,
					uploadProgress: 0,
					uploadComplete: true
				});
			},
			(error) => {
				self.setState({
					uploading: 		false,
					uploadProgress: 0,
					uploadComplete: false
				});
			},
			(percent) => {
				self.setState({uploadProgress: percent});
			}
		);
	}
	
	uploadSeriesPhoto = () => {
		const self = this;
		uploadFile(
			'images',
			this.state.foto_serie,
			(url) => {
				self.setState({foto_serie: url});
				self.setState({
					uploading: 		false,
					uploadProgress: 0,
					uploadComplete: true
				});
			},
			(error) => {
				self.setState({
					uploading: 		false,
					uploadProgress: 0,
					uploadComplete: false
				});
			},
			(percent) => {
				self.setState({uploadProgress: percent});
			}
		);
	}
	
	saveIssue = (data) => {
		const self			=	this;
		const id			=	this.state.key;
		const reference		=	firebase.firestore().collection("inventario").where("key", "==", id)
		reference
		.get()
		.then(function(querySnapshot){
			if(querySnapshot.docs.length > 0){
				const doc = querySnapshot.docs[0];
				doc.ref.update(data)
				.then(function(){
					AsyncStorage.setItem("Inventory-"+id, JSON.stringify(data))
					.then(function(){
						self.setState({modalVisible: false});
						self.setState(data);
					})
					.catch(function(error){
						
					})
				})
				.catch(function(error){
					
				});
			}
		})
		.catch(function(error){
			
		});
	}
	
	addIssue = () => {
		const self			=	this;
		const old_data		=	this.props.navigation.state.params.data;
		this.setState({modalVisible: true});
		if(old_data.foto != this.state.foto && this.state.foto != ""){
			this.uploadMainPhoto();
		}
		if(old_data.foto_serie != this.state.foto_serie && this.state.foto_serie != ""){
			this.uploadSeriesPhoto();
		}
		self.saveIssue({
			key:		this.state.key,
			nombre: 	this.state.nombre,
			serie: 		this.state.serie,
			foto:		this.state.foto,
			estado:		this.state.estado,
			area:		this.state.area,
			cantidad:	this.state.cantidad,
			marca:		this.state.marca,
			modelo:		this.state.modelo,
			foto_serie:	this.state.foto_serie,
			inUse:		this.state.inUse,
		});
	}
	
	render() {
		const navigation	= this.props.navigation;
		const btnDisabled	= !(this.state.nombre.trim() != "");
		return (
			<SafeAreaView style={{ width: '100%', height: '100%'}}>
				<Modal
					animationType="slide"
					transparent={false}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						
					}}
				>
					<View style={{width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center'}}>
						<Image source={ImageReady} style={{width: '50%', resizeMode: 'contain'}}/>						
						<View style={{width: '100%', height: 50, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
							<Text>{"Guardando..." + " " + this.state.uploadProgress + "%"}</Text>
						</View>
					</View>
				</Modal>
				<View style={{width: '100%', height: 50, flexDirection: 'row', alignItems: 'center', padding: 5, backgroundColor: 'white', elevation: 1}}>
					<Icon
						Borderless
						name="arrow_left"
						onPress={() => {
							navigation.state.params.onGoBack();
							navigation.goBack();
						}}
					/>
					<Text style={{marginLeft: 10}}> Añadir registro</Text>
					<View style={{alignItems: 'center', position: 'absolute', right: 10}}>
						<Button title="Guardar" disabled={btnDisabled} onPress={this.addIssue}/>
					</View>
				</View>
				<ScrollView contentContainerStyle={{padding: 10}}>
					<View>
						<Text>Por favor, completa los siguientes campos:</Text>
					</View>
					<React.Fragment>
						<TextInput
							placeholder="Nombre"
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({nombre: text})}}
							value={this.state.nombre}
						/>
					</React.Fragment>
					<React.Fragment>
						<TextInput
							placeholder="Número de serie"
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({serie: text})}}
							value={this.state.serie}
						/>
					</React.Fragment>
					<React.Fragment>
						<TextInput
							placeholder="Marca"
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({marca: text})}}
							value={this.state.marca}
						/>
					</React.Fragment>
					<React.Fragment>
						<TextInput
							placeholder="Modelo"
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({modelo: text})}}
							value={this.state.modelo}
						/>
					</React.Fragment>
					<React.Fragment>
						<TextInput
							placeholder="Cantidad"
							keyboardType="number-pad"
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({cantidad: text})}}
							value={(this.state.cantidad).toString()}
						/>
					</React.Fragment>
					<React.Fragment>
						<TextInput
							placeholder="Área"
							style={{borderBottomWidth: 1, borderBottomColor: 'gray'}}
							onChangeText={(text) => {this.setState({area: text})}}
							value={this.state.area}
						/>
					</React.Fragment>
					<View
						style={{height: '40%', margin: 20, marginTop: 50, alignItems: 'center'}}
					>
						<View style={{marginBottom: 20, width: '100%', borderBottomWidth: 1/PixelRatio.get(), borderBottomColor: '#CCCCCC', alignItems: 'center', justifyContent: 'center'}}>
							<View
								style={{
									width: 300,
									height: 150,
									backgroundColor: ((this.state.foto_serie == "" || this.state.foto_serie == null) ? '#EEEEEE' : 'transparent'),
									alignItems: 'center', 
									justifyContent: 'center'
								}}
							>
								{(this.state.foto_serie != "" && this.state.foto_serie != null) &&
									<Image source={{uri: this.state.foto_serie}} style={{width: 300, height: 150, resizeMode: 'contain'}}/>
								}
								{(this.state.foto_serie == "" || this.state.foto_serie == null) &&
									<View>
										<Text>Agrega la fotografía del número de serie.</Text>
									</View>
								}
							</View>
							<View style={{marginTop: 20, borderBottomWidth: 1/PixelRatio.get(), borderBottomColor: 'rgba(200, 200, 200, 0.75)', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', height: 50}}>
								<TouchableOpacity
									onPress={()=>{this.setState({foto_serie: ''})}}
									style={{borderWidth: 1, borderRadius: 10, borderColor: 'gray', padding: 5, width: '30%', }}
								>
									<Text style={{textAlign: 'center'}}>Eliminar</Text>
								</TouchableOpacity>
								<TouchableOpacity
									onPress={this.pickImageSeries}
									style={{borderWidth: 1, borderRadius: 10, borderColor: 'gray', padding: 5, width: '30%'}}
								>
									<Text style={{textAlign: 'center'}}>Seleccionar</Text>
								</TouchableOpacity>
							</View>
						</View>
						<View style={{marginBottom: 20, width: '100%', borderBottomWidth: 1/PixelRatio.get(), borderBottomColor: '#CCCCCC', alignItems: 'center', justifyContent: 'center'}}>
							<View
								style={{
									width: 300,
									height: 300,
									backgroundColor: ((this.state.foto == "" || this.state.foto == null) ? '#EEEEEE' : 'transparent'),
									alignItems: 'center', 
									justifyContent: 'center'
								}}
							>
								{(this.state.foto != "" && this.state.foto != null) &&
									<Image source={{uri: this.state.foto}} style={{width: 300, height: 300, resizeMode: 'contain'}}/>
								}
								{(this.state.foto == "" || this.state.foto == null) &&
									<View>
										<Text>Agrega una fotografía al inventario.</Text>
									</View>
								}
							</View>
							<View style={{marginTop: 20, borderBottomWidth: 1/PixelRatio.get(), borderBottomColor: 'rgba(200, 200, 200, 0.75)', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', height: 50}}>
								<TouchableOpacity
									onPress={()=>{this.setState({foto: ''})}}
									style={{borderWidth: 1, borderRadius: 10, borderColor: 'gray', padding: 5, width: '30%', }}
								>
									<Text style={{textAlign: 'center'}}>Eliminar</Text>
								</TouchableOpacity>
								<TouchableOpacity
									onPress={this.pickImage}
									style={{borderWidth: 1, borderRadius: 10, borderColor: 'gray', padding: 5, width: '30%'}}
								>
									<Text style={{textAlign: 'center'}}>Seleccionar</Text>
								</TouchableOpacity>
							</View>
						</View>
						<View style={{width: '100%', height: 50, flexDirection: 'row', marginTop: 20}}>
							<TouchableOpacity
								onPress={() => { this.setState({estado: 0}) }}
								style={{
									width: '33.3%',
									height: 50,
									backgroundColor: (this.state.estado == 0 ? InventoryStatus[0].color : 'transparent'),
									borderBottomLeftRadius: 10,
									borderTopLeftRadius: 10,
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<Text
									style={{
										textAlign: 'center',
										color: (this.state.estado == 0 ? 'white' : 'black')
									}}
								>{InventoryStatus[0].label}</Text>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => { this.setState({estado: 1}) }}
								style={{
									width: '33.4%',
									height: 50,
									backgroundColor: (this.state.estado == 1 ? InventoryStatus[1].color : 'transparent'),
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<Text
									style={{
										textAlign: 'center',
										color: (this.state.estado == 1 ? 'white' : 'black')
									}}
								>{InventoryStatus[1].label}</Text>
							</TouchableOpacity>
							<TouchableOpacity
								onPress={() => { this.setState({estado: 2}) }}
								style={{
									width: '33.3%',
									height: 50,
									backgroundColor: (this.state.estado == 2 ? InventoryStatus[2].color : 'transparent'),
									borderBottomRightRadius: 10,
									borderTopRightRadius: 10,
									alignItems: 'center',
									justifyContent: 'center'
								}}
							>
								<Text
									style={{
										textAlign: 'center',
										color: (this.state.estado == 2 ? 'white' : 'black')
									}}
								>{InventoryStatus[2].label}</Text>
							</TouchableOpacity>
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}