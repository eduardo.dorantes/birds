const admin_emails = [
	"emhgerencia@gmail.com",
	"eduardo.dorantes.07@gmail.com"
];

const isAdmin = (email) => {
	return admin_emails.includes(email);
}
export default isAdmin;