const defaultGray		= "#BCBEC0";
const defaultGrayLight	= "#F2F2F2";
const defaultGrayBold   = "rgb(80, 80, 80)";

export default {
	defaultGrayLight,
	defaultGray,
	defaultGrayBold,
};
