const InventoryStatus = {
	0: {label: 'OK', color: 'green'},
	1: {label: 'En reparación', color: '#017EE5'},
	2: {label: 'Dañado', color: 'red'}
}

export default InventoryStatus;