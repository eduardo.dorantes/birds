## B.I.R.D.S.
###### Biomedical Issue Reporting Digital System

![B.I.R.D.S.](https://firebasestorage.googleapis.com/v0/b/birds-772c7.appspot.com/o/preview%2Fbird.png?alt=media&token=d478c373-a1f7-4341-9438-88b0c625fc17 "B.I.R.D.S.")


------------


### Features

- Real-time viewing data.
- Unlimited reports.
- Attach HQ photos to your reports.

### B.I.R.D.S.

It is a simple real-time react-native application with an approach to a to-do list app, you must sign up in order to use the app.

With B.I.R.D.S. you can store unlimited _issue reports_ composed by:
- Title
- Issue
- Solution
- Photo
- Status

Once you save the report it also attaches the following data:
- Owner
- Date
- Key

The app requires the following permissions:
- Camera
- External Storage

And also relies on AsyncStorage (for caching) and Google's Firebase to show you real-time data.
