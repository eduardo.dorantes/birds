import React from 'react';
import { Image, StyleSheet, View, PixelRatio, TouchableOpacity } from 'react-native';
import {icons} from '../../constants/Icons';
import Colors from '../../constants/Colors';

export default class Icon extends React.Component{
	constructor(props){
		super(props);
		this.PressHandler = this.PressHandler.bind(this);
	}
	
	PressHandler = () => {
		if(this.props.onPress){
			this.props.onPress();
		}
	}
	
	
	render(){
		const size = this.props.size ? this.props.size : 40;
		const border = this.props.Borderless ? 0 : 0.75;
		const background = this.props.background ? this.props.background : "transparent";
		const color = this.props.forceColor ? this.props.color : this.props.background ? 'white' : Colors.defaultGray;
		const name = this.props.name ? this.props.name : "unknown";
		const MainComponent = this.props.onPress ? TouchableOpacity : View;
		const factor = this.props.factor ? this.props.factor : 0.6;
		const borderColor = this.props.borderColor ? this.props.borderColor : "#CCCCCC";
		return(
			<MainComponent
				onPress={this.PressHandler}
				style={{
					borderRadius: 100,
					borderColor: borderColor,
					backgroundColor: 'white',
					width: size,
					height: size,
					borderWidth: border,
					backgroundColor: background,
					elevation: this.props.Shadow ? 2 : 0
				}
			}>
				<View style={{
					flex: 1,
					flexDirection: 'column',
					alignItems: 'center',
					justifyContent: 'center',
				}}>
				{!this.props.Colorless && 
					<Image style={{
						resizeMode: 'contain',
						width: (size*factor),
						height: (size*factor),
						tintColor: color
					}} source={icons[name]}/>
				}
				{this.props.Colorless && 
					<Image style={{
						resizeMode: 'contain',
						width: (size*0.6),
						height: (size*0.6)
					}} source={icons[name]}/>
				}
				</View>
			</MainComponent>
		);
	}
}