import React from 'react';
import { View, Image, Text, TouchableOpacity, Alert } from 'react-native';
import Icon from '../Icon';
import Layout from '../../constants/Layout';
import firebase from 'react-native-firebase';

const ProfilePicture = require('../../assets/pictures/user.png');

export default class Header extends React.Component{
	constructor(props){
		super(props);
		this.signOut = this.signOut.bind(this);
	}
	
	signOut = async() => {
		Alert.alert(
			"Salir",
			"¿Quieres salir de la aplicación?",
			[
				{
					text: "No",
					onPress: () => {},
				},
				{
					text: "Sí",
					onPress: async() => {
						await firebase.auth().signOut();
					}
				}
			],
			{cancelable: true}
		)
	}
	
	render(){
		const name = firebase.auth().currentUser.displayName;
		return(
			<View style={{width: '100%', height: 50, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#CCCCCC'}}>
				<View>
					<View style={{width: 30, height: 30, borderRadius: 360, backgroundColor: '#CCCCC', marginLeft: 10}}>
						<Image source={ProfilePicture} style={{width: 30, height: 30, resizeMode: 'contain'}}/>
					</View>
				</View>
				<View>
					<Text style={{marginLeft: 10, width: Layout.window.width - 120}}>{name}</Text>
				</View>
				<View style={{width: 50, height: 50, position: 'absolute', right: 0, alignItems: 'center', justifyContent: 'center'}}>
					<TouchableOpacity onPress={this.signOut}>
						<Text style={{}}>Salir</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}