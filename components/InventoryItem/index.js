import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import InventoryStatus from '../../constants/InventoryStatus';

export default class InventoryItem extends React.Component{
	constructor(props){
		super(props);
		this.onPress = this.onPress.bind(this);
	}
	
	componentDidMount(){
		
	}
	
	componentWillUnmount(){
		
	}
	
	onPress = () => {
		this.props.onPress();
	}
	
	render(){
		const data = this.props.data;
		return(
			<TouchableOpacity onPress={this.onPress} style={{width: (Layout.window.width), height: 50, flexDirection: 'row'}}>
				<View style={{width: 50, height: 50, alignItems: 'center', justifyContent: 'center'}}>
					<View style={{width: 40, height: 40, backgroundColor: Colors.defaultGrayLight, borderRadius: 10}}>
						{(data.foto != "") &&
							<Image source={{uri: data.foto}} style={{width: 40, height: 40, resizeMode: 'cover'}}/>
						}
					</View>
				</View>
				<View style={{justifyContent: 'center'}}>
					<Text numberOfLines={1} style={{width: Layout.window.width - 50 - 100, fontWeight: 'bold'}}>{data.nombre}</Text>
					<Text numberOfLines={1} style={{width: Layout.window.width - 50 - 100}}>{data.serie}</Text>
				</View>
				<View style={{alignItems: 'center', justifyContent: 'center'}}>
					<View style={{margin: 1, alignItems: 'center', justifyContent: 'center', width: 90, height: 15, backgroundColor: InventoryStatus[data.estado].color, borderRadius: 5}}>
						<Text style={{textAlign: 'center', width: 90, color: 'white', fontSize: 10}}>{InventoryStatus[data.estado].label}</Text>
					</View>
					<View style={{margin: 1, alignItems: 'center', justifyContent: 'center', width: 90, height: 	15, backgroundColor: (data.inUse ? 'gray' : 'green'), borderRadius: 5}}>
						<Text style={{textAlign: 'center', width: 90, color: 'white', fontSize: 10}}>{(data.inUse ? 'En uso' : 'Disponible')}</Text>
					</View>
				</View>
			</TouchableOpacity>
		);
	}
}