import React from 'react';
import { createStackNavigator, createAppContainer, createSwitchNavigator, createBottomTabNavigator } from "react-navigation";
import Icon from '../components/Icon';
import AuthScreen from '../screens/AuthScreen';
import IssueListScreen from '../screens/IssueListScreen';
import IssueDetailScreen from '../screens/IssueDetailScreen';
import IssueNewScreen from '../screens/IssueNewScreen';
import IssueEditScreen from '../screens/IssueEditScreen';

import InventoryMainScreen from '../screens/InventoryMainScreen';
import InventoryDetailsScreen from '../screens/InventoryDetailsScreen';
import InventoryEditScreen from '../screens/InventoryEditScreen';
import InventoryAddScreen from '../screens/InventoryAddScreen';


import AdminMainScreen from '../screens/AdminMainScreen';

const IssueNavigator = createStackNavigator({
	IssueList:		IssueListScreen,
	IssueDetail:	IssueDetailScreen,
	IssueNew:		IssueNewScreen,
	IssueEdit:		IssueEditScreen,
},
{
	headerMode: 'none',
	navigationOptions: ({ navigation }) => ({
		tabBarLabel: 'Reportes',
		tabBarVisible: navigation.state.index < 1,
		tabBarIcon: ({ focused }) => (
			<Icon Borderless forceColor={focused} color={'black'} name="list" factor={0.75}/>
		)
	})
});

const InventoryNavigator = createStackNavigator({
	InventoryMain:		InventoryMainScreen,
	InventoryDetails:	InventoryDetailsScreen,
	InventoryEdit:		InventoryEditScreen,
	InventoryAdd:		InventoryAddScreen
}, {
	headerMode: 'none',
	navigationOptions: ({ navigation }) => ({
		tabBarLabel: 'Inventario',
		tabBarVisible: navigation.state.index < 1,
		tabBarIcon: ({ focused }) => (
			<Icon Borderless forceColor={focused} color={'black'} name="inventory" factor={0.75}/>
		)
	})
});

const AdminNavigator = createStackNavigator({
	AdminMain:		AdminMainScreen,
	InventoryDetails:	InventoryDetailsScreen,
	InventoryEdit:		InventoryEditScreen,
	InventoryAdd:		InventoryAddScreen
},{
	headerMode: 'none',
	navigationOptions: ({ navigation }) => ({
		tabBarLabel: 'Admin',
		tabBarVisible: navigation.state.index < 1,
		tabBarIcon: ({ focused }) => (
			<Icon Borderless forceColor={focused} color={'black'} name="personal" factor={0.75}/>
		)
	})
});

const AppNavigator = createBottomTabNavigator({
	IssueNavigator,
	InventoryNavigator,
	AdminNavigator
}, { tabBarOptions: { activeTintColor: 'black' }});

export default createAppContainer(AppNavigator);