import firebase from 'react-native-firebase';
import uuid from 'uuid/v4';

const uploadFile = async (collection, path, onSuccess, onError, onProgress) => {
	const ext			=	path.split('.').pop();
	const filename		=	`${uuid()}.${ext}`;
	const ref			=	firebase.storage().ref(`${collection}/${filename}`);
	var done			=	false
	const unsubscribe	= ref.putFile(path).on(
		firebase.storage.TaskEvent.STATE_CHANGED,
		(snapshot) => {
			if(typeof onProgress != "undefined"){
				const progress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
				onProgress(progress);
			}
			if (snapshot.state === firebase.storage.TaskState.SUCCESS) {
				if(!done){
					onSuccess(snapshot.downloadURL);
					done = true;
				}
			}
		},
		(error) => {
			unsubscribe();
			if(typeof onError != "undefined"){
				onError(error);
			}
		},
	);
}

export {
	uploadFile
};